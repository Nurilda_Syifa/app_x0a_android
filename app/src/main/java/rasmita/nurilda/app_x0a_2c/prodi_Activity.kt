package rasmita.nurilda.app_x0a_2c

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_prod.*
import org.json.JSONArray
import org.json.JSONObject

class prodi_Activity : AppCompatActivity(), View.OnClickListener{

    lateinit var prodiAdapter : Adapter_prodi
    var daftarProdi = mutableListOf<HashMap<String,String>>()
    var IDProdi = ""
    val mainUrl = "http://192.168.43.159/kampus/"
    val url = mainUrl+"data_prodi.php"
    val url2 = mainUrl+"query_prodi.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btninp ->{
                queryProdi("insert")
            }
            R.id.btnupd ->{
                queryProdi("update")
            }
            R.id.btnhps ->{
                queryProdi("delete")
            }
            R.id.btnfindprodi-> {
                showDataProdi(Ednmprod.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prod)
        prodiAdapter = Adapter_prodi(daftarProdi,this)
        listprodi.layoutManager = LinearLayoutManager(this)
        listprodi.adapter = prodiAdapter
        btninp.setOnClickListener(this)
        btnupd.setOnClickListener(this)
        btnhps.setOnClickListener(this)
        btnfindprodi.setOnClickListener(this)
    }

    fun showDataProdi(nmProdi : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prodi = HashMap<String,String>()
                    prodi.put("id_prodi",jsonObject.getString("id_prodi"))
                    prodi.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    daftarProdi.add(prodi)
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_prodi",nmProdi)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryProdi(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataProdi("")
                    clearInput()
                }
                else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("id_prodi",IDProdi)
                        hm.put("nama_prodi",Ednmprod.text.toString())
                    }
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("nama_prodi",Ednmprod.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_prodi",IDProdi)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataProdi("")
    }

    fun clearInput(){
        IDProdi = ""
        Ednmprod.setText("")
    }
}