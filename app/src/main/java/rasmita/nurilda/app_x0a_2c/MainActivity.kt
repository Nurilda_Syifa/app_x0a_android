package rasmita.nurilda.app_x0a_2c



import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity(), View.OnClickListener{
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMhs -> {
                var mhs = Intent(this, MhsActivity::class.java)
                startActivity(mhs)
                true
            }
            R.id.btnProdi -> {
                var prd = Intent(this, prodi_Activity::class.java)
                startActivity(prd)
                true

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnMhs.setOnClickListener(this)
        btnProdi.setOnClickListener(this)
    }






}
