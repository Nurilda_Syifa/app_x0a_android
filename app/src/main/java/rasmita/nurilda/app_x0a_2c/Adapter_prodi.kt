package rasmita.nurilda.app_x0a_2c

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_prod.*

class Adapter_prodi (val dataProdi : List<HashMap<String,String>>, val ProdiActivity: prodi_Activity) :
    RecyclerView.Adapter<Adapter_prodi.HolderDataProdi>(){

    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): Adapter_prodi.HolderDataProdi {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_prodi,p0,false)
        return  HolderDataProdi(v)
    }

    override fun getItemCount(): Int {
        return dataProdi.size
    }

    override fun onBindViewHolder(holder: Adapter_prodi.HolderDataProdi, position: Int) {
        val data = dataProdi.get(position)
        holder.txtNmProdi.setText(data.get("nama_prodi"))
        if(position.rem(2) == 0) holder.prodi1.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.prodi1.setBackgroundColor(Color.rgb(255,255,245))

        holder.prodi1.setOnClickListener({
            ProdiActivity.IDProdi = data.get("id_prodi").toString()
            ProdiActivity.Ednmprod.setText(data.get("nama_prodi"))
        })
    }

    class HolderDataProdi(v: View) : RecyclerView.ViewHolder(v) {
        val txtNmProdi = v.findViewById<TextView>(R.id.prodi)
        val prodi1 = v.findViewById<ConstraintLayout>(R.id.prodi1)
    }
}